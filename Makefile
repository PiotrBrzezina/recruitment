#!/usr/bin/env make

-include .env
export

default: bash

start:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down

logs:
	docker-compose logs --timestamps --tail 25 --follow

bash:
	docker-compose up -d php
	docker-compose exec php sh

docker-build-image:
	docker build --no-cache --target php_app --file ./docker/Dockerfile --tag piotrbrzezina/gizer_php_app:latest .
	docker build --target nginx_app --file ./docker/Dockerfile --tag piotrbrzezina/gizer_nginx_app:latest .

docker-push-image:
	docker tag piotrbrzezina/gizer_php_app:latest piotrbrzezina/gizer_php_app:${DOCKER-IMAGE-VERSION}
	docker tag piotrbrzezina/gizer_nginx_app:latest piotrbrzezina/gizer_nginx_app:${DOCKER-IMAGE-VERSION}
	docker push piotrbrzezina/gizer_php_app:${DOCKER-IMAGE-VERSION}
	docker push piotrbrzezina/gizer_nginx_app:${DOCKER-IMAGE-VERSION}

phpspec-run:
	docker-compose up -d php
	docker-compose exec -T php vendor/bin/phpspec run --no-code-generation --ansi

behat:
	docker-compose up -d php
	docker-compose exec -T php vendor/bin/behat

phpunit:
	docker-compose up -d php
	docker-compose exec -T php vendor/bin/phpunit

phpunit-infrastructure:
	docker-compose up -d php database
	sleep 30
	docker-compose exec -T php bin/console doctrine:mongodb:schema:update
	docker-compose exec -T php vendor/bin/phpunit -c ./phpunit-infrastructure.xml.dist

mutagen-enable:
	@echo "Enable mutagen"
	@make down
	sed '/^ *- .:\/opt\/app/s/^/#/' docker-compose.override.yaml> mutagen.tmp && cat mutagen.tmp > docker-compose.override.yaml
	cp -n ./mutagen.yml.dist ./mutagen.yml || :
	sed 's/{directory-name}/'$(notdir $(CURDIR))'/g' mutagen.yml > mutagen.tmp && cat mutagen.tmp > mutagen.yml
	rm mutagen.tmp;
	@make start
	mutagen project terminate || :
	mutagen project start || :

mutagen-disable:
	@echo "Disable mutagen"
	mutagen project terminate || :
	sed '/^# *- .:\/opt\/app/s/^#//' docker-compose.override.yaml> mutagen.tmp && cat mutagen.tmp > docker-compose.override.yaml
	rm mutagen.tmp;
	@make down
	@make start

ecs:
	@echo "running ECS"
	docker-compose run --rm phpqa ecs check src --config ecs.yaml

ecs-fix:
	@echo "fixing "
	docker-compose run --rm phpqa ecs check src --config ecs.yaml --fix

phpmd:
	@echo "Analyzing (phpmd):"
	docker-compose run --rm phpqa phpmd . text codesize,unusedcode --exclude vendor/,var/,bin/,migrations/,spec/,tests/

phpstan:
	@echo "Analyzing (phpstan):"
	docker-compose run --rm phpqa phpstan analyse -c phpstan.neon -l 7 src

setup:
	cp -n ./docker-compose.override.yaml.dist ./docker-compose.override.yaml || :
	docker-compose up --force-recreate --always-recreate-deps -d --remove-orphans --build
	sleep 30
	docker-compose exec -T php bin/console doctrine:mongodb:schema:update