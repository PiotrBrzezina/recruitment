<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\ThirdPartApi;

use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository as GameScoreRepositoryInterface;
use Gizer\Recruitment\Tests\unit\Infrastructure\ThirdPartApi\Exception\InvalidSortFieldException;
use Symfony\Component\Serializer\SerializerInterface;

class GameScoreRepository implements GameScoreRepositoryInterface
{
    private HttpClient $httpClient;
    private RequestFactory $requestFactory;
    private SerializerInterface $serializer;

    public function __construct(
        HttpClient $httpClient,
        RequestFactory $requestFactory,
        SerializerInterface $serializer
    ) {
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory;
        $this->serializer = $serializer;
    }

    public function findById(int $gameId, ?GameScoreOrderBy $orderBy): ?GameScoreCollection
    {
        $response = $this->httpClient->send($this->requestFactory->getGameScore($gameId));

        if (null === $response) {
            return null;
        }

        $data = $this->serializer->deserialize(
            $response->getBody()->getContents(),
            GameScore::class.'[]',
            'json'
        );

        if ($orderBy instanceof GameScoreOrderBy) {
            usort($data, function (GameScore $a, GameScore $b) use ($orderBy) {
                $a = $this->getObjectValue($a, 'get'.ucfirst($orderBy->getOrderBy()));
                $b = $this->getObjectValue($b, 'get'.ucfirst($orderBy->getOrderBy()));

                return 'ASC' === $orderBy->getDirection() ? $a > $b : $a < $b;
            });
        }

        return new GameScoreCollection($data);
    }

    /**
     * @param object $object
     * @param string $methodToCall
     *
     * @return mixed
     */
    private function getObjectValue(Object $object, string $methodToCall)
    {
        $callable = [$object, $methodToCall];

        if (is_callable($callable) && method_exists($object, $methodToCall)) {
            return call_user_func($callable);
        }

        throw new InvalidSortFieldException(sprintf('Method "%s::%s()" not found', get_class($object), $methodToCall));
    }
}
