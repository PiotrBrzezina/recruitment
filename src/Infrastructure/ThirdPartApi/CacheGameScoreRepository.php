<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\ThirdPartApi;

use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository as GameScoreRepositoryInterface;
use Gizer\Recruitment\Infrastructure\Cache\CacheInterface;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;

class CacheGameScoreRepository implements GameScoreRepositoryInterface
{
    private GameScoreRepositoryInterface $gameScoreRepository;
    private CacheInterface $cache;
    private int $cacheTTL;

    public function __construct(GameScoreRepositoryInterface $gameScoreRepository, CacheInterface $cache, int $cacheTTL)
    {
        $this->gameScoreRepository = $gameScoreRepository;
        $this->cache = $cache;
        $this->cacheTTL = $cacheTTL;
    }

    public function findById(int $gameId, ?GameScoreOrderBy $gameScoreOrderBy): ?GameScoreCollection
    {
        $orderDirection = $orderBy = null;
        if ($gameScoreOrderBy instanceof GameScoreOrderBy) {
            $orderDirection = $gameScoreOrderBy->getDirection();
            $orderBy = $gameScoreOrderBy->getOrderBy();
        }
        $cachedItem = $this->cache->getItem($this->getCacheKey($gameId), $orderBy, $orderDirection);
        if (null !== $cachedItem) {
            return new GameScoreCollection($cachedItem->getCachedValue()->toArray());
        }

        $gameScoreCollection = $this->gameScoreRepository->findById($gameId, $gameScoreOrderBy);
        if (null === $gameScoreCollection) {
            return null;
        }

        $this->cache->save(new CacheItem(
            $this->getCacheKey($gameId),
            $gameScoreCollection,
            new \DateTimeImmutable("+{$this->cacheTTL} sec")
        ));

        return $gameScoreCollection;
    }

    private function getCacheKey(int $gameId): string
    {
        return 'GameScore'.$gameId;
    }
}
