<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\ThirdPartApi;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestFactory
{
    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function getGameScore(int $gameId): RequestInterface
    {
        return new Request(
            'GET',
            sprintf('%s/results/games/%d', $this->baseUrl, $gameId)
        );
    }
}
