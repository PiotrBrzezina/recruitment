<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\ThirdPartApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GuzzleHttpClient implements HttpClient
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send(RequestInterface $request): ?ResponseInterface
    {
        try {
            return $this->client->send($request, []);
        } catch (ClientException $exception) {
            return null;
        }
    }
}
