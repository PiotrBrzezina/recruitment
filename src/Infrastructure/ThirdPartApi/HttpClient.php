<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\ThirdPartApi;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpClient
{
    public function send(RequestInterface $request): ?ResponseInterface;
}
