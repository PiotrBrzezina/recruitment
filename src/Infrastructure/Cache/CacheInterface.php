<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Cache;

use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;

interface CacheInterface
{
    public function getItem(string $key, ?string $orderBy, ?string $direction): ?CacheItem;

    public function save(CacheItem $item): void;
}
