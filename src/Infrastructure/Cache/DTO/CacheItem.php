<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Cache\DTO;

use Doctrine\Common\Collections\Collection;

class CacheItem
{
    private string $key;
    private Collection $cachedValue;
    private \DateTimeInterface $expireAt;

    public function __construct(string $key, Collection $cachedValue, \DateTimeInterface $expireAt)
    {
        $this->key = $key;
        $this->cachedValue = $cachedValue;
        $this->expireAt = $expireAt;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return Collection
     */
    public function getCachedValue(): Collection
    {
        return $this->cachedValue;
    }

    public function getExpireAt(): \DateTimeInterface
    {
        return $this->expireAt;
    }
}
