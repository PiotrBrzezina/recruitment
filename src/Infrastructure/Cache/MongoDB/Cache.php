<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Cache\MongoDB;

use Gizer\Recruitment\Infrastructure\Cache\CacheInterface;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use Gizer\Recruitment\Infrastructure\Cache\MongoDB\Repository\CacheItemRepository;

class Cache implements CacheInterface
{
    private CacheItemRepository $cacheRepository;

    public function __construct(CacheItemRepository $cacheRepository)
    {
        $this->cacheRepository = $cacheRepository;
    }

    public function getItem(string $key, ?string $orderBy, ?string $direction): ?CacheItem
    {
        if (null !== $orderBy) {
            return $this->cacheRepository->findAndSort($key, $orderBy, 'ASC' === $direction ? 1 : -1);
        }

        return $this->cacheRepository->find($key);
    }

    public function save(CacheItem $item): void
    {
        $this->cacheRepository->save($item);
    }
}
