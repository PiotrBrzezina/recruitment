<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Cache\MongoDB\Repository;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\LockMode;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;

/**
 * @method find($id, int $lockMode = LockMode::NONE, ?int $lockVersion = null) : ?CacheItem
 */
class CacheItemRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CacheItem::class);
    }

    public function save(CacheItem $item): void
    {
        $this->getDocumentManager()->persist($item);
        $this->getDocumentManager()->flush();
    }

    public function findAndSort(string $key, string $sortBy, int $sortDirection): ?CacheItem
    {
        $builder = $this->getDocumentManager()->createAggregationBuilder(CacheItem::class)
            ->hydrate(CacheItem::class)
            ->match()
            ->field('_id')
            ->equals($key)
            ->unwind('$cachedValue')
            ->sort(["cachedValue.${sortBy}" => $sortDirection])
            ->group()
            ->field('_id')
            ->expression('$_id')
            ->field('expireAt')
            ->first('$expireAt')
            ->field('cachedValue')
            ->push('$cachedValue');

        $data = $builder->execute()->current();

        return $data instanceof CacheItem ? $data : null;
    }
}
