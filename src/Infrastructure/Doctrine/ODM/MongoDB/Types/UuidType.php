<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Doctrine\ODM\MongoDB\Types;

use Doctrine\ODM\MongoDB\Types\ClosureToPHP;
use Doctrine\ODM\MongoDB\Types\Type;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidType extends Type
{
    use ClosureToPHP;

    public function convertToPHPValue($value): UuidInterface
    {
        return Uuid::fromString($value);
    }

    /**
     * @param UuidInterface $value
     *
     * @return string
     */
    public function convertToDatabaseValue($value): string
    {
        return $value->toString();
    }
}
