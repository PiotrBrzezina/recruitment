<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\DTO;

class GameScoreOrderBy
{
    /**
     * @var mixed
     */
    public $orderBy;

    /**
     * @var mixed
     */
    public $direction;
}
