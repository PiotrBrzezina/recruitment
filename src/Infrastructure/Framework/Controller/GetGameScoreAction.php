<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Controller;

use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository;
use Gizer\Recruitment\Infrastructure\Framework\DTO\GameScoreOrderBy as GameScoreOrderByDTO;
use Gizer\Recruitment\Infrastructure\Framework\Response\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GetGameScoreAction
{
    private GameScoreRepository $gameScoreRepository;
    private NormalizerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(
        GameScoreRepository $gameScoreRepository,
        NormalizerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->gameScoreRepository = $gameScoreRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function __invoke(int $gameId, GameScoreOrderByDTO $gameScoreOrderByDTO): Response
    {
        $errors = $this->validator->validate($gameScoreOrderByDTO);
        if (count($errors) > 0) {
            return new Response((array) $this->serializer->normalize($errors), 422);
        }

        $gameScoreOrderBy = null;
        if (null !== $gameScoreOrderByDTO->orderBy) {
            $gameScoreOrderBy = new GameScoreOrderBy($gameScoreOrderByDTO->orderBy, $gameScoreOrderByDTO->direction);
        }

        $gameResult = $this->gameScoreRepository->findById($gameId, $gameScoreOrderBy);

        if (null === $gameResult) {
            return new Response(['status' => 'Not found'], 404);
        }

        return new Response((array) $this->serializer->normalize($gameResult));
    }
}
