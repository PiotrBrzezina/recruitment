<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Response;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse
{
    private ?array $data;

    public function __construct(?array $data = [], int $status = 200, array $headers = [])
    {
        parent::__construct('', $status, $headers);

        $this->data = $data;
    }

    public function getData(): ?array
    {
        return $this->data;
    }
}
