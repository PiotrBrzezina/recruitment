<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat;

class ResponseContentNegotiator
{
    private ResponseEncoderInterface $fallbackTransformer;

    /**
     * @var ResponseEncoderInterface[]
     */
    private array $transformers = [];

    public function __construct(iterable $transformers, ResponseEncoderInterface $fallbackTransformer)
    {
        $this->fallbackTransformer = $fallbackTransformer;
        foreach ($transformers as $transformer) {
            if (!$transformer instanceof ResponseEncoderInterface) {
                throw new \Exception(sprintf('class \"%s\" should implement "%s"', get_class($transformer), ResponseEncoderInterface::class));
            }
            $this->transformers[] = $transformer;
        }
    }

    public function transform(array $data, ?string $format): string
    {
        foreach ($this->transformers as $transformer) {
            if ($transformer->support($format)) {
                return $transformer->format($data);
            }
        }

        return $this->fallbackTransformer->format($data);
    }

    public function getContentType(?string $format): string
    {
        foreach ($this->transformers as $transformer) {
            if ($transformer->support($format)) {
                return $transformer->getContentType();
            }
        }

        return $this->fallbackTransformer->getContentType();
    }
}
