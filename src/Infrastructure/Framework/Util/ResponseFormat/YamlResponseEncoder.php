<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat;

use Symfony\Component\Serializer\SerializerInterface;

class YamlResponseEncoder implements ResponseEncoderInterface
{
    private const CONTENT_TYPE = 'application/x-yaml';
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function support(?string $format): bool
    {
        return $format === static::CONTENT_TYPE;
    }

    public function format(array $data): string
    {
        return $this->serializer->serialize($data, 'yaml');
    }

    public function getContentType(): string
    {
        return static::CONTENT_TYPE;
    }
}
