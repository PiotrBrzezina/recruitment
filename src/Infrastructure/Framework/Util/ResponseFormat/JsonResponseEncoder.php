<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat;

class JsonResponseEncoder implements ResponseEncoderInterface
{
    private const CONTENT_TYPE = 'application/json';

    public function support(?string $format): bool
    {
        return $format === static::CONTENT_TYPE;
    }

    public function format(array $data): string
    {
        return (string) json_encode($data);
    }

    public function getContentType(): string
    {
        return static::CONTENT_TYPE;
    }
}
