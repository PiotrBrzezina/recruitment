<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat;

interface ResponseEncoderInterface
{
    public function support(?string $format): bool;

    public function format(array $data): string;

    public function getContentType(): string;
}
