<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Serializer;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UuidDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): ?UuidInterface
    {
        if (null === $data) {
            return null;
        }

        return Uuid::fromString($data);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, string $type, ?string $format = null): bool
    {
        return (Uuid::class === $type || UuidInterface::class === $type) && $this->isValid($data);
    }

    /**
     * @param mixed $data
     *
     * @return bool
     */
    private function isValid($data): bool
    {
        return null === $data
            || (is_string($data) && Uuid::isValid($data));
    }
}
