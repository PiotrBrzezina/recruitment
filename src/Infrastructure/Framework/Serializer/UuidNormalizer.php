<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\Serializer;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UuidNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, ?string $format = null, array $context = []): string
    {
        return $object->toString();
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, ?string $format = null): bool
    {
        return $data instanceof UuidInterface;
    }
}
