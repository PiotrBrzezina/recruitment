<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\EventSubscriber;

use Gizer\Recruitment\Infrastructure\Framework\Response\Response;
use Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat\ResponseContentNegotiator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class RequestSubscriber implements EventSubscriberInterface
{
    private ResponseContentNegotiator $contentNegotiator;
    private ?string $requestAccept;

    public function __construct(ResponseContentNegotiator $contentNegotiator)
    {
        $this->contentNegotiator = $contentNegotiator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onRequest',
            ResponseEvent::class => 'onResponse',
        ];
    }

    public function onRequest(RequestEvent $event): void
    {
        $this->requestAccept = $event->getRequest()->headers->get('Accept');
    }

    public function onResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();
        if ($response instanceof Response) {
            $response->setContent(
                $this->contentNegotiator->transform($response->getData(), $this->requestAccept)
            );

            if (!$response->headers->has('Content-Type')) {
                $response->headers->add([
                    'Content-Type' => $this->contentNegotiator->getContentType($this->requestAccept),
                ]);
            }
        }
    }
}
