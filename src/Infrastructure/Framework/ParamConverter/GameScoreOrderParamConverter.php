<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Framework\ParamConverter;

use Gizer\Recruitment\Infrastructure\Framework\DTO\GameScoreOrderBy;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class GameScoreOrderParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration)
    {
        $DTO = new GameScoreOrderBy();
        $DTO->direction = $request->query->get('direction', 'ASC');
        $DTO->orderBy = $this->convertToCamelCase($request->query->get('orderBy'));

        $request->attributes->set($configuration->getName(), $DTO);

        return true;
    }

    public function supports(ParamConverter $configuration)
    {
        return GameScoreOrderBy::class === $configuration->getClass();
    }

    private function convertToCamelCase(?string $path): ?string
    {
        if (null === $path) {
            return null;
        }

        return lcfirst(preg_replace_callback('/(^|_|\.)+(.)/', function ($match) {
            return ('.' === $match[1] ? '_' : '').strtoupper($match[2]);
        }, $path));
    }
}
