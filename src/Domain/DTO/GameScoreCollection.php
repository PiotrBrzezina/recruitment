<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\DTO;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class GameScoreCollection extends ArrayCollection implements Collection
{
}
