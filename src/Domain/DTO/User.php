<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\DTO;

use Ramsey\Uuid\UuidInterface;

class User
{
    private UuidInterface $id;
    private string $name;

    public function __construct(UuidInterface $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
