<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\DTO;

use Ramsey\Uuid\UuidInterface;

class GameScore
{
    private UuidInterface $id;
    private int $score;
    private \DateTimeInterface $finishedAt;
    private User $user;

    public function __construct(UuidInterface $id, int $score, \DateTimeInterface $finishedAt, User $user)
    {
        $this->id = $id;
        $this->score = $score;
        $this->finishedAt = $finishedAt;
        $this->user = $user;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getFinishedAt(): \DateTimeInterface
    {
        return $this->finishedAt;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
