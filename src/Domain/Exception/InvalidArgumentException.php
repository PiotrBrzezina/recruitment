<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\Exception;

use Throwable;

class InvalidArgumentException extends \InvalidArgumentException implements DomainException
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
