<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\Repository;

use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;

interface GameScoreRepository
{
    public function findById(int $gameId, ?GameScoreOrderBy $orderBy): ?GameScoreCollection;
}
