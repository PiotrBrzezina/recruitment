<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Domain\Model;

use Gizer\Recruitment\Domain\Exception\InvalidArgumentException;

class GameScoreOrderBy
{
    public const FIELDS = ['finishedAt', 'score'];
    public const DIRECTION = ['ASC', 'DESC'];

    private string $orderBy;
    private string $direction;

    public function __construct(string $orderBy, string $direction)
    {
        $this->guardOrderBy($orderBy);
        $this->guardDirection($direction);

        $this->orderBy = $orderBy;
        $this->direction = $direction;
    }

    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    private function guardOrderBy(string $orderBy): void
    {
        if (!in_array($orderBy, self::FIELDS, true)) {
            throw new InvalidArgumentException("Can not sort by \"${orderBy}\" allowed values are: { implode(' ,', self::FIELDS) } ");
        }
    }

    private function guardDirection(string $direction): void
    {
        if (!in_array($direction, self::DIRECTION, true)) {
            throw new InvalidArgumentException("cannot sort in \"${direction}\" direction allowed directions are: { implode(' ,', self::DIRECTION) } ");
        }
    }
}
