<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\infrastructure\ThirdPartApi;


use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Infrastructure\ThirdPartApi\GameScoreRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GameScoreRepositoryTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function testGetByIdForeExistingGame()
    {
        $this->mockHttpClient(new Response(200, ['Content-Type' => 'application/json'], '[{"id":"a227380b-890b-4265-b26a-d5c8849c281a","user":{"name":"Leona Everett","id":"9f4139ac-1b7a-43e2-95e3-a94f94b17571"},"score":5,"finished_at":"2020-02-27T11:25:00+00:00"},{"id":"2a708bc2-452e-4826-8b07-69653181d178","user":{"name":"Erica Harmon","id":"d5d97477-fb05-492a-abe7-ec62d5a71333"},"score":20,"finished_at":"2020-02-27T01:30:00+10:00"}]'));

        /** @var GameScoreRepository $queryHandler */
        $repository = self::$container->get(GameScoreRepository::class);
        $this->assertCount(2, $repository->findById(1, null));
    }

    public function testGetByIdForeExistingGameWithSortingByFinishedAt()
    {
        $this->mockHttpClient(new Response(200, ['Content-Type' => 'application/json'], '[{"id":"a227380b-890b-4265-b26a-d5c8849c281a","user":{"name":"Leona Everett","id":"9f4139ac-1b7a-43e2-95e3-a94f94b17571"},"score":5,"finished_at":"2020-02-27T11:25:00+00:00"},{"id":"2a708bc2-452e-4826-8b07-69653181d178","user":{"name":"Erica Harmon","id":"d5d97477-fb05-492a-abe7-ec62d5a71333"},"score":20,"finished_at":"2020-02-27T01:30:00+10:00"}]'));

        /** @var GameScoreRepository $queryHandler */
        $repository = self::$container->get(GameScoreRepository::class);
        $result = $repository->findById(1, new GameScoreOrderBy('finishedAt', 'ASC'));

        $this->assertCount(2, $result);
        $this->assertEquals(new \DateTimeImmutable('2020-02-27T01:30:00+10:00'), $result->first()->getFinishedAt());
    }

    public function testGetByIdForeExistingGameWithSortingByScore()
    {
        $this->mockHttpClient(new Response(200, ['Content-Type' => 'application/json'], '[{"id":"a227380b-890b-4265-b26a-d5c8849c281a","user":{"name":"Leona Everett","id":"9f4139ac-1b7a-43e2-95e3-a94f94b17571"},"score":5,"finished_at":"2020-02-27T11:25:00+00:00"},{"id":"2a708bc2-452e-4826-8b07-69653181d178","user":{"name":"Erica Harmon","id":"d5d97477-fb05-492a-abe7-ec62d5a71333"},"score":20,"finished_at":"2020-02-27T01:30:00+10:00"}]'));

        /** @var GameScoreRepository $queryHandler */
        $repository = self::$container->get(GameScoreRepository::class);
        $result = $repository->findById(1, new GameScoreOrderBy('score', 'DESC'));

        $this->assertCount(2, $result);
        $this->assertEquals(20, $result->first()->getScore());
    }

    public function testGetByIdForeNotExistingGame()
    {
        $this->mockHttpClient(new Response(404, [], '{}'));

        /** @var GameScoreRepository $queryHandler */
        $repository = self::$container->get(GameScoreRepository::class);
        $this->assertNull($repository->findById(1, null));
    }

    private function mockHttpClient(Response $expectedResponse)
    {
        $mock = new MockHandler([
            $expectedResponse,
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        self::$container->set(Client::class, $client);
    }
}
