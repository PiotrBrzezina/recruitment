<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Infrastructure\Cache\MongoDB\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\User;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use Gizer\Recruitment\Infrastructure\ThirdPartApi\GameScoreRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CacheItemRepositoryTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $documentManager = self::$container->get(DocumentManager::class);
        $documentManager->getSchemaManager()->dropDocumentCollection(CacheItem::class);
    }

    public function testSaveCacheItem(): void
    {
        $cacheKey = 'TestCacheItem1';

        /** @var GameScoreRepository $queryHandler */
        $repository = self::$container->get(CacheItemRepository::class);
        $repository->save($this->creatCacheItem($cacheKey, new ArrayCollection([$this->creatGameScore()])));

        $documentManager = self::$container->get(DocumentManager::class);
        $documentManager->clear();
        $cacheItem = $documentManager->createQueryBuilder(CacheItem::class)
            ->field('key')->equals($cacheKey)
            ->getQuery()
            ->getSingleResult();

        $this->assertEquals($cacheKey, $cacheItem->getKey());
    }

    public function testFindCachedItem(): void
    {
        $cacheKey = 'TestCacheItem2';

        /** @var GameScoreRepository $queryHandler */
        $documentManager = self::$container->get(DocumentManager::class);
        $documentManager->persist($this->creatCacheItem($cacheKey, new ArrayCollection([$this->creatGameScore()])));
        $documentManager->flush();
        $documentManager->clear();

        $repository = self::$container->get(CacheItemRepository::class);
        $cachedItem = $repository->find($cacheKey);

        $this->assertEquals($cacheKey, $cachedItem->getKey());
    }

    public function testFindCachedItemWithSortingByFinishedAt(): void
    {
        $cacheKey = 'TestCacheItem3';

        /** @var GameScoreRepository $queryHandler */
        $documentManager = self::$container->get(DocumentManager::class);
        $cachedItems = new ArrayCollection([
            $this->creatGameScore(30, '-30 min'),
            $this->creatGameScore(10, '-10 min'),
            $this->creatGameScore(20, '-20 min'),
        ]);
        $documentManager->persist($this->creatCacheItem($cacheKey, $cachedItems));
        $documentManager->flush();
        $documentManager->clear();

        $repository = self::$container->get(CacheItemRepository::class);
        $cachedItem = $repository->findAndSort($cacheKey, 'finishedAt', -1);
        $this->assertEquals($cacheKey, $cachedItem->getKey());
        $this->assertEquals(10, $cachedItem->getCachedValue()->first()->getScore());
    }

    public function testFindCachedItemWithSortingByScore(): void
    {
        $cacheKey = 'TestCacheItem4';

        /** @var GameScoreRepository $queryHandler */
        $documentManager = self::$container->get(DocumentManager::class);
        $cachedItems = new ArrayCollection([
            $this->creatGameScore(30, '-30 min'),
            $this->creatGameScore(10, '-10 min'),
            $this->creatGameScore(20, '-20 min'),
        ]);
        $documentManager->persist($this->creatCacheItem($cacheKey, $cachedItems));
        $documentManager->flush();
        $documentManager->clear();

        $repository = self::$container->get(CacheItemRepository::class);
        $cachedItem = $repository->findAndSort($cacheKey, 'score', 1);

        $this->assertEquals($cacheKey, $cachedItem->getKey());
        $this->assertEquals(30, $cachedItem->getCachedValue()->last()->getScore());
    }

    private function creatCacheItem(string $cacheKey, ArrayCollection $cachedItems): CacheItem
    {
        return new CacheItem($cacheKey, $cachedItems, new \DateTimeImmutable('+10 sec'));
    }

    private function creatGameScore(int $score = 3, string $finishedAt = 'now'): GameScore
    {
        return new GameScore(Uuid::uuid4(), $score, new \DateTimeImmutable($finishedAt), new User(Uuid::uuid4(), 'User1'));
    }
}
