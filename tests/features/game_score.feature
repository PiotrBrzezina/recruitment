Feature: Game score
  As an api client
  I want to get game result

  Scenario: Get game scores
    Given there are scores for game 1 available
    When I send a "GET" request to "/api/v1/game/1/score" with information that response should be "application/json"
    Then the response status code should be 200
    And the header "Content-Type" should be equal to "application/json"
    And the response should be in JSON
    And the response should match:
    """
    [
      {
        "id": "@uuid@",
        "score": "@integer@",
        "finished_at": "@string@.isDateTime()",
        "user": {
          "id": "@uuid@",
          "name": "@string@"
        }
      },
      "@...@"
    ]
    """

  Scenario: Get sorted game scores
    Given there are scores for game 1 available
    When I send a "GET" request to "/api/v1/game/1/score?orderBy=score&direction=DESC" with information that response should be "application/x-yaml"
    Then the response status code should be 200
    And the header "Content-Type" should be equal to "application/x-yaml"
    And the response should be in YAML
    And the yaml response should match:
    """
    [
      {
        "id": "@uuid@",
        "score": "@integer@",
        "finished_at": "@string@.isDateTime()",
        "user": {
          "id": "@uuid@",
          "name": "@string@"
        }
      },
      "@...@"
    ]
    """

  Scenario: trying to get game scores with wrong sorting parameters
    When I send a "GET" request to "/api/v1/game/1/score?orderBy=wrongField&direction=wrongDirection" with information that response should be "application/json"
    Then the response status code should be 422
    And the header "Content-Type" should be equal to "application/json"
    And the response should be in JSON
    And the response should match:
    """
   {
      "type": "https://symfony.com/errors/validation",
      "title": "Validation Failed",
      "detail": "@string@",
      "violations": [
        {
          "propertyPath": "@string@",
          "title": "@string@",
          "parameters": @...@,
          "type": "@string@"
        },
        @...@
      ]
    }
    """