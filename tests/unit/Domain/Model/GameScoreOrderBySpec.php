<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Domain\Model;

use Gizer\Recruitment\Domain\Exception\InvalidArgumentException;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GameScoreOrderBySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('finishedAt', 'DESC');
        $this->getOrderBy()->shouldBe('finishedAt');
        $this->getDirection()->shouldBe('DESC');
    }

    function it_should_throw_exception_if_we_set_wrong_order_by_value()
    {
        $this->beConstructedWith('wrongOrderBy', 'DESC');
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_should_throw_exception_if_we_set_wrong_direction_value()
    {
        $this->beConstructedWith('score', 'wrongDirection');
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }
}
