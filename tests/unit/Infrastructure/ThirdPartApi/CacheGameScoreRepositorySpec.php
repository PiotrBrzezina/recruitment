<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\ThirdPartApi;

use Doctrine\Common\Collections\ArrayCollection;
use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository as GameScoreRepositoryInterface;
use Gizer\Recruitment\Infrastructure\Cache\CacheInterface;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CacheGameScoreRepositorySpec extends ObjectBehavior
{
    private int $cacheTTL = 10;

    function let(GameScoreRepositoryInterface $gameScoreRepository, CacheInterface $cache)
    {
        $this->beConstructedWith($gameScoreRepository, $cache, $this->cacheTTL);
    }

    function it_should_implement_game_score_repository_interface()
    {
        $this->shouldBeAnInstanceOf(GameScoreRepositoryInterface::class);
    }

    function it_should_return_cached_response(
        CacheInterface $cache,
        GameScore $gameScore,
        GameScoreRepositoryInterface $gameScoreRepository
    ) {
        $gameId = 1;
        $cacheKey = 'GameScore' . $gameId;
        $cachedItems = new CacheItem(
            $cacheKey,
            new ArrayCollection([$gameScore->getWrappedObject()]),
            new \DateTimeImmutable('+10 sec')
        );

        $cache->getItem($cacheKey, null, null)->willReturn($cachedItems);
        $gameScoreRepository->findById(Argument::any(), Argument::any())->shouldNotBeCalled();

        $result = $this->findById($gameId, null);
        $result->shouldBeAnInstanceOf(GameScoreCollection::class);
        $result->first()->shouldBe($gameScore);
    }


    function it_should_return_sorted_cached_response(
        CacheInterface $cache,
        GameScore $gameScore,
        GameScoreRepositoryInterface $gameScoreRepository
    ) {
        $gameId = 1;
        $cacheKey = 'GameScore' . $gameId;
        $gameScoreOrderBy = new GameScoreOrderBy('finishedAt', 'ASC');
        $cachedItems = new CacheItem(
            $cacheKey,
            new ArrayCollection([$gameScore->getWrappedObject()]),
            new \DateTimeImmutable('+10 sec')
        );

        $cache->getItem($cacheKey, 'finishedAt', 'ASC')->willReturn($cachedItems);
        $gameScoreRepository->findById(Argument::any(), Argument::any())->shouldNotBeCalled();

        $result = $this->findById($gameId, $gameScoreOrderBy);
        $result->shouldBeAnInstanceOf(GameScoreCollection::class);
        $result->first()->shouldBe($gameScore);
    }

    function it_should_fetch_data_from_real_repository_and_add_it_in_to_cache_if_there_are_no_result_in_cache(
        CacheInterface $cache,
        GameScoreRepositoryInterface $gameScoreRepository
    ) {
        $gameId = 1;
        $cacheKey = 'GameScore' . $gameId;
        $gameScoreOrderBy = new GameScoreOrderBy('finishedAt', 'ASC');
        $gameScoreCollection = new GameScoreCollection();
        $cache->getItem($cacheKey, 'finishedAt', 'ASC')->willReturn(null);
        $gameScoreRepository->findById($gameId, $gameScoreOrderBy)->willReturn($gameScoreCollection);
        $cache->save(Argument::that(function ($definitions) use ($cacheKey, $gameScoreCollection) {
            if ($definitions->getKey() === $cacheKey && $definitions->getCachedValue() === $gameScoreCollection) {
                return true;
            }
            return false;
        }))->shouldBeCalled();

        $this->findById($gameId, $gameScoreOrderBy)->shouldBe($gameScoreCollection);
    }

    function it_should_return_null_if_there_are_no_result_for_give_game_id(
        CacheInterface $cache,
        GameScoreRepositoryInterface $gameScoreRepository
    ) {
        $gameId = 1;
        $cacheKey = 'GameScore' . $gameId;

        $cache->getItem($cacheKey, null, null)->willReturn(null);
        $gameScoreRepository->findById($gameId, null)->willReturn(null);

        $this->findById($gameId, null)->shouldBe(null);
    }
}
