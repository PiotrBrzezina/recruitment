<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\ThirdPartApi;


use Gizer\Recruitment\Infrastructure\ThirdPartApi\HttpClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GuzzleHttpClientSpec extends ObjectBehavior
{
    function let(Client $client)
    {
        $this->beConstructedWith($client);
    }

    function it_should_implement_http_client_interface()
    {
        $this->shouldBeAnInstanceOf(HttpClient::class);
    }

    function it_should_be_send_request_and_return_response(
        Client $client,
        RequestInterface $request,
        ResponseInterface $response
    ) {
        $client->send($request, [])->willReturn($response);

        $this->send($request)->shouldBe($response);
    }

    function it_should_return_null_if_there_is_an_client_error(
        Client $client,
        RequestInterface $request,
        ResponseInterface $response
    ) {
        $client->send($request, [])->willThrow(new ClientException('message', $request->getWrappedObject(), $response->getWrappedObject()));

        $this->send($request)->shouldBe(null);
    }
}
