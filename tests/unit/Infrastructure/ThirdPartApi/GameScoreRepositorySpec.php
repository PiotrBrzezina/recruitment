<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\ThirdPartApi;


use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository as GameScoreRepositoryInterface;
use Gizer\Recruitment\Infrastructure\ThirdPartApi\RequestFactory;
use Gizer\Recruitment\Infrastructure\ThirdPartApi\HttpClient;
use Gizer\Recruitment\Tests\unit\Infrastructure\ThirdPartApi\Exception\InvalidSortFieldException;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\Serializer\SerializerInterface;

class GameScoreRepositorySpec extends ObjectBehavior
{
    function let(
        HttpClient $client,
        RequestFactory $requestFactory,
        SerializerInterface $serializer,
        RequestInterface $request,
        ResponseInterface $response,
        StreamInterface $stream,
        GameScore $gameScore1,
        GameScore $gameScore2
    ) {
        $this->beConstructedWith($client, $requestFactory, $serializer);

        $requestFactory->getGameScore(1)->willReturn($request);
        $client->send($request)->willReturn($response);
        $response->getBody()->willReturn($stream);
        $stream->getContents()->willReturn('{"data":"dummy"}');
        $serializer->deserialize('{"data":"dummy"}', GameScore::class . '[]', 'json')->willReturn([$gameScore1, $gameScore2]);

    }

    function it_should_implement_game_score_repository_interface()
    {
        $this->shouldBeAnInstanceOf(GameScoreRepositoryInterface::class);
    }

    function it_should_be_able_to_fetch_data_from_external_api(
        GameScore $gameScore1
    ) {

        $data = $this->findById(1, null);
        $data->shouldBeAnInstanceOf(GameScoreCollection::class);
        $data->first()->shouldBe($gameScore1);
    }

    function it_should_return_null_if_there_are_no_result_for_give_game_id(
        HttpClient $client,
        RequestFactory $requestFactory,
        RequestInterface $request
    ) {
        $requestFactory->getGameScore(1)->willReturn($request);
        $client->send($request)->willReturn(null);

        $data = $this->findById(1, null)->shouldBe(null);
    }

    function it_should_be_able_sort_fetched_data_by_score(
        GameScore $gameScore1,
        GameScore $gameScore2
    ) {
        $gameScore1->getScore()->willReturn(5);
        $gameScore2->getScore()->willReturn(10);

        $order = new GameScoreOrderBy('score', 'ASC');
        $data = $this->findById(1, $order);
        $data->first()->shouldBe($gameScore1);

        $order = new GameScoreOrderBy('score', 'DESC');
        $data = $this->findById(1, $order);
        $data->first()->shouldBe($gameScore2);
    }

    function it_should_be_able_sort_fetched_data_by_finishedAt(
        GameScore $gameScore1,
        GameScore $gameScore2
    ) {
        $gameScore1->getFinishedAt()->willReturn(new \DateTimeImmutable('-10 min'));
        $gameScore2->getFinishedAt()->willReturn(new \DateTimeImmutable('-5 min'));

        $order = new GameScoreOrderBy('finishedAt', 'ASC');
        $data = $this->findById(1, $order);
        $data->first()->shouldBe($gameScore1);

        $order = new GameScoreOrderBy('finishedAt', 'DESC');
        $data = $this->findById(1, $order);
        $data->first()->shouldBe($gameScore2);
    }

    function it_should_throw_error_if_there_is_not_chance_to_sort_by_given_field(
        GameScoreOrderBy $orderBy
    ) {
        $orderBy->getOrderBy()->willReturn('unExistingField');
        $this->shouldThrow(InvalidSortFieldException::class)->during('findById', [1, $orderBy]);
    }
}
