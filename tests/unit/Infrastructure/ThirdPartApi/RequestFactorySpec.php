<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\ThirdPartApi;


use PhpSpec\ObjectBehavior;

class RequestFactorySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('https://fake_url');
    }

    function it_should_create_request_to_get_game_score()
    {
        $request = $this->getGameScore(1);

        $request->getMethod()->shouldBe('GET');
        $request->getUri()->getPath()->shouldBe('/results/games/1');
        $request->getUri()->getHost()->shouldBe('fake_url');
    }
}
