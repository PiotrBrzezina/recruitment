<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\Cache\MongoDB;

use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use Gizer\Recruitment\Infrastructure\Cache\MongoDB\Repository\CacheItemRepository;
use PhpSpec\ObjectBehavior;

class CacheSpec extends ObjectBehavior
{
    function let(CacheItemRepository $cacheRepository)
    {
        $this->beConstructedWith($cacheRepository);
    }

    function it_should_be_able_to_save_item(CacheItemRepository $cacheRepository, CacheItem $cacheItem)
    {
        $cacheRepository->save($cacheItem)->shouldBeCalled();

        $this->save($cacheItem);
    }

    function it_should_be_able_return_cached_item(CacheItemRepository $cacheRepository, CacheItem $cacheItem)
    {
        $cacheKey = 'DummyKey';
        $cacheRepository->find($cacheKey)->willReturn($cacheItem);

        $this->getItem($cacheKey, null, null)->shouldBe($cacheItem);
    }

    function it_should_be_able_return_cached_item_sorted_in_descending_order(
        CacheItemRepository $cacheRepository,
        CacheItem $cacheItem
    ) {
        $cacheKey = 'DummyKey';
        $orderBy = 'finishedAt';
        $direction = 'DESC';

        $cacheRepository->findAndSort($cacheKey, $orderBy, -1)->willReturn($cacheItem);

        $this->getItem($cacheKey, $orderBy, $direction)->shouldBe($cacheItem);
    }

    function it_should_be_able_return_cached_item_sorted_in_ascending_order(
        CacheItemRepository $cacheRepository,
        CacheItem $cacheItem
    ) {
        $cacheKey = 'DummyKey';
        $orderBy = 'finishedAt';
        $direction = 'ASC';

        $cacheRepository->findAndSort($cacheKey, $orderBy, 1)->willReturn($cacheItem);

        $this->getItem($cacheKey, $orderBy, $direction)->shouldBe($cacheItem);
    }
}
