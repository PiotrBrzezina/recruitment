<?php

declare(strict_types=1);

namespace unit\Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat;

use Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat\ResponseContentNegotiator;
use Gizer\Recruitment\Infrastructure\Framework\Util\ResponseFormat\ResponseEncoderInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ResponseContentNegotiatorSpec extends ObjectBehavior
{
    function let(
        ResponseEncoderInterface $encoder1,
        ResponseEncoderInterface $encoder2,
        ResponseEncoderInterface $encoder3,
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $encoder1->support(Argument::any())->willReturn(false);
        $encoder2->support(Argument::any())->willReturn(false);
        $encoder3->support(Argument::any())->willReturn(false);
        $fallbackEncoder->support(Argument::any())->shouldNotBeCalled();

        $this->beConstructedWith([$encoder1, $encoder2, $encoder3], $fallbackEncoder);
    }

    function it_should_throw_exception_if_passed_collection_of_encoders_contain_object_that_does_not_implement_ResponseEncoderInterface(
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $this->beConstructedWith([new class() {
        }], $fallbackEncoder);
        $this->shouldThrow(\Exception::class)->duringInstantiation();
    }

    function it_should_use_first_encoder_that_are_able_to_encode_response_to_given_format(
        ResponseEncoderInterface $encoder1,
        ResponseEncoderInterface $encoder2,
        ResponseEncoderInterface $encoder3,
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $data = ['dummyData'];
        $transformedData = 'dummyDataInYaml';
        $format = 'yaml';

        $encoder1->format(Argument::any())->shouldNotBeCalled();
        $encoder3->format(Argument::any())->shouldNotBeCalled();
        $fallbackEncoder->format(Argument::any())->shouldNotBeCalled();

        $encoder2->support($format)->willReturn(true);
        $encoder2->format($data)->willReturn($transformedData);

        $this->transform($data, $format)->shouldBe($transformedData);
    }

    function it_should_use_fallback_encoder_to_transform_if_none_of_given_encoders_are_not_able_to_handle_given_format(
        ResponseEncoderInterface $encoder1,
        ResponseEncoderInterface $encoder2,
        ResponseEncoderInterface $encoder3,
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $data = ['dummyData'];
        $transformedData = 'dummyDataFromFallback';
        $format = 'notSupportedFormat';

        $encoder1->format($format)->shouldNotBeCalled();
        $encoder2->format($format)->shouldNotBeCalled();
        $encoder3->format($format)->shouldNotBeCalled();
        $fallbackEncoder->format(Argument::any())->willReturn($transformedData);

        $this->transform($data, $format)->shouldBe($transformedData);
    }

    function it_should_return_content_type_base_on_given_format(
        ResponseEncoderInterface $encoder1,
        ResponseEncoderInterface $encoder2,
        ResponseEncoderInterface $encoder3,
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $contentType = 'application/x-yaml';
        $format = 'yaml';

        $encoder1->getContentType()->shouldNotBeCalled();
        $encoder3->getContentType()->shouldNotBeCalled();
        $fallbackEncoder->getContentType()->shouldNotBeCalled();

        $encoder2->support($format)->willReturn(true);
        $encoder2->getContentType()->willReturn($contentType);

        $this->getContentType($format)->shouldBe($contentType);
    }

    function it_should_use_fallback_encoder_to_get_content_type_if_none_of_given_encoders_are_not_able_to_handle_given_format(
        ResponseEncoderInterface $encoder1,
        ResponseEncoderInterface $encoder2,
        ResponseEncoderInterface $encoder3,
        ResponseEncoderInterface $fallbackEncoder
    ) {
        $contentType = 'application/notSupportedFormat';
        $format = 'notSupportedFormat';

        $encoder1->getContentType()->shouldNotBeCalled();
        $encoder3->getContentType()->shouldNotBeCalled();
        $encoder2->getContentType()->shouldNotBeCalled();

        $fallbackEncoder->getContentType()->willReturn($contentType);

        $this->getContentType($format)->shouldBe($contentType);
    }
}
