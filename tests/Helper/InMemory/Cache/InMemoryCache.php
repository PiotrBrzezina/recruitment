<?php
declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\InMemory\Cache;


use Doctrine\Common\Collections\ArrayCollection;
use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Infrastructure\Cache\CacheInterface;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use Gizer\Recruitment\Tests\unit\Infrastructure\ThirdPartApi\Exception\InvalidSortFieldException;

class InMemoryCache implements CacheInterface
{
    /**
     * @var array|CacheItem[]
     */
    public static array $cache = [];

    public function getItem(string $key, ?string $orderBy, ?string $direction): ?CacheItem
    {
        if (array_key_exists($key, self::$cache) && self::$cache[$key]->getExpireAt() > new \DateTimeImmutable()) {
            $cachedItem = self::$cache[$key];
            if ($orderBy !== null) {
                $cachedItem = new CacheItem(
                    $cachedItem->getKey(),
                    new GameScoreCollection($this->sort($cachedItem->getCachedValue()->toArray(), $orderBy, $direction)),
                    $cachedItem->getExpireAt()
                );
            }

            return $cachedItem;
        }

        return null;
    }

    public function save(CacheItem $item): void
    {
        self::$cache[$item->getKey()] = $item;
    }

    public function clear(): void
    {
        self::$cache = [];
    }

    public function countItemInCache(): int
    {
        return count(self::$cache);
    }

    private function sort(array $data, string $orderBy, string $direction): array
    {
        usort($data, function (GameScore $a, GameScore $b) use ($orderBy, $direction) {

            $a = $this->getObjectValue($a, 'get' . ucfirst($orderBy));
            $b = $this->getObjectValue($b, 'get' . ucfirst($orderBy));

            return 'ASC' === $direction ? $a > $b : $a < $b;
        });

        return $data;
    }

    /**
     * @param Object $object
     * @param string $methodToCall
     * @return mixed
     *
     * @throws \Exception
     */
    private function getObjectValue(Object $object, string $methodToCall)
    {
        $callable = [$object, $methodToCall];

        if (is_callable($callable)) {
            return call_user_func($callable);
        }

        throw new \Exception(sprintf('Method "%s::%s()" not found', get_class($object), $methodToCall));
    }
}
