<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\InMemory\Repository;


use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\GameScoreCollection;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository;

class InMemoryGameScoreRepository implements GameScoreRepository
{
    /**
     * @var array|GameScore[]
     */
    private static array $gameScores = [];

    public function findById(int $gameId, ?GameScoreOrderBy $orderBy): ?GameScoreCollection
    {
        if (array_key_exists($gameId, self::$gameScores)) {
            $data = self::$gameScores[$gameId];
            if ($orderBy instanceof GameScoreOrderBy) {
                $data = $this->sort($data, $orderBy->getOrderBy(), $orderBy->getDirection());
            }

            return new GameScoreCollection($data);
        }

        return null;
    }

    public function addGameScore(GameScore $gameScore, int $gameId): void
    {
        self::$gameScores[$gameId][] = $gameScore;
    }

    public function clear(): void
    {
        self::$gameScores = [];
    }

    private function sort(array $data, string $orderBy, string $direction): array
    {
        usort($data, function (GameScore $a, GameScore $b) use ($orderBy, $direction) {

            $a = $this->getObjectValue($a, 'get' . ucfirst($orderBy));
            $b = $this->getObjectValue($b, 'get' . ucfirst($orderBy));

            return 'ASC' === $direction ? $a > $b : $a < $b;
        });

        return $data;
    }

    /**
     * @param Object $object
     * @param string $methodToCall
     * @return mixed
     *
     * @throws \Exception
     */
    private function getObjectValue(Object $object, string $methodToCall)
    {
        $callable = [$object, $methodToCall];

        if (is_callable($callable)) {
            return call_user_func($callable);
        }

        throw new \Exception(sprintf('Method "%s::%s()" not found', get_class($object), $methodToCall));
    }
}
