<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\BehatContext;


use Behat\Gherkin\Node\PyStringNode;
use Behatch\Context\BaseContext;
use Behatch\HttpCall\HttpCallResultPool;
use Behatch\Json\JsonInspector;
use Coduo\PHPMatcher\Factory\MatcherFactory;
use Coduo\PHPMatcher\Matcher;
use Symfony\Component\Serializer\Encoder\YamlEncoder;

class YamlContext extends BaseContext
{
    private Matcher $matcher;
    private YamlEncoder $decoder;
    private HttpCallResultPool $httpCallResultPool;

    public function __construct(HttpCallResultPool $httpCallResultPool)
    {
        $this->httpCallResultPool = $httpCallResultPool;

        $factory = new MatcherFactory();
        $this->matcher = $factory->createMatcher();

        $this->decoder = new YamlEncoder();
    }

    /**
     * @Then the yaml response should match:
     * @param PyStringNode $string
     */
    public function theResponseShouldMatch(PyStringNode $string)
    {
        if (!$this->matcher->match(
            json_encode($this->decoder->decode($this->httpCallResultPool->getResult()->getValue(), 'yaml')),
            $string->getRaw()
        )) {
            throw new \LogicException($this->matcher->getError());
        }
    }

    /**
     * @Given the response should be in YAML
     */
    public function theResponseShouldBeInYAML()
    {
        $this->decoder->decode($this->httpCallResultPool->getResult()->getValue(), 'yaml');
    }
}
