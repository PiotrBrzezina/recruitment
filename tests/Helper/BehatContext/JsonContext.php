<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\BehatContext;


use Behat\Gherkin\Node\PyStringNode;
use Behatch\Context\JsonContext as BaseJsonContext;
use Behatch\HttpCall\HttpCallResultPool;
use Coduo\PHPMatcher\Factory\MatcherFactory;
use Coduo\PHPMatcher\Matcher;
use Coduo\PHPMatcher\Matcher\ValueMatcher;

class JsonContext extends BaseJsonContext
{
    private Matcher $matcher;

    public function __construct(HttpCallResultPool $httpCallResultPool, $evaluationMode = 'javascript')
    {
        parent::__construct($httpCallResultPool, $evaluationMode);

        $factory = new MatcherFactory();
        $this->matcher = $factory->createMatcher();
    }

    /**
     * @Then the response should match:
     * @param PyStringNode $string
     */
    public function theResponseShouldMatch(PyStringNode $string)
    {
        if (!$this->matcher->match(
            $this->getJson()->encode(),
            $string->getRaw()
        )) {
            throw new \LogicException($this->matcher->getError());
        }
    }
}
