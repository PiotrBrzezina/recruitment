<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\BehatContext;


use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Tester\Exception\PendingException;
use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\User;
use Gizer\Recruitment\Tests\Helper\InMemory\Repository\InMemoryGameScoreRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\DateTime;

class FixtureContext implements Context
{
    private InMemoryGameScoreRepository $gameScoreRepository;

    public function __construct(
        InMemoryGameScoreRepository $gameScoreRepository
    ) {
        $this->gameScoreRepository = $gameScoreRepository;
    }

    /**
     * @AfterScenario
     *
     * @param AfterScenarioScope $event
     */
    public function clearRepository(AfterScenarioScope $event)
    {
        $this->gameScoreRepository->clear();
    }

    /**
     * @Given there are scores for game :gameID available
     */
    public function thereExistScoresForGameWithId(int $gameId)
    {
        $this->gameScoreRepository->addGameScore(new GameScore(Uuid::uuid4(), 4, new \DateTimeImmutable('-4 days'), new User(Uuid::uuid4(), 'User1')), $gameId);
        $this->gameScoreRepository->addGameScore(new GameScore(Uuid::uuid4(), 5, new \DateTimeImmutable('-3 days'), new User(Uuid::uuid4(), 'User2')), $gameId);
        $this->gameScoreRepository->addGameScore(new GameScore(Uuid::uuid4(), 3, new \DateTimeImmutable('-2 days'), new User(Uuid::uuid4(), 'User3')), $gameId);
    }
}
