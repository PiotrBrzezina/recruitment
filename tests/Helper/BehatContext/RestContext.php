<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\Helper\BehatContext;


use Behat\Gherkin\Node\PyStringNode;
use Behatch\Context\RestContext as BaseRestContext;

class RestContext extends BaseRestContext
{
    /**
     * Sends a HTTP request
     *
     * @Given I send a :method request to :url with information that response should be :format
     */
    public function iSendARequestToWithResponseFormat($method, $url, $format, PyStringNode $body = null, $files = [])
    {
        $this->iAddHeaderEqualTo('Accept', $format);
        return $this->iSendARequestTo($method, $url, $body);
    }
}
