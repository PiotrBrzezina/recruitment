<?php

declare(strict_types=1);

namespace Gizer\Recruitment\Tests\integration;


use Doctrine\Common\Collections\ArrayCollection;
use Gizer\Recruitment\Domain\DTO\GameScore;
use Gizer\Recruitment\Domain\DTO\User;
use Gizer\Recruitment\Domain\Model\GameScoreOrderBy;
use Gizer\Recruitment\Domain\Repository\GameScoreRepository;
use Gizer\Recruitment\Infrastructure\Cache\DTO\CacheItem;
use Gizer\Recruitment\Tests\Helper\InMemory\Cache\InMemoryCache;
use Gizer\Recruitment\Tests\Helper\InMemory\Repository\InMemoryGameScoreRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GameScoreRepositoryTest extends KernelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        self::$container->get(InMemoryGameScoreRepository::class)->clear();
        self::$container->get(InMemoryCache::class)->clear();
    }

    public function testGetGameResultFromThirdPartApi(): void
    {
        $gameId = 1;
        /** @var InMemoryGameScoreRepository $inMemoryGameScoreRepository */
        $inMemoryGameScoreRepository = self::$container->get(InMemoryGameScoreRepository::class);
        $inMemoryGameScoreRepository->addGameScore($this->creatGameScore(), $gameId);

        /** @var InMemoryCache $inMemoryCache */
        $inMemoryCache = self::$container->get(InMemoryCache::class);

        $gameScoreRepository = self::$container->get(GameScoreRepository::class);

        $this->assertCount(1, $gameScoreRepository->findById($gameId, null));
        $this->assertEquals($inMemoryCache->countItemInCache(), 1);
    }

    public function testGetGameResultFromThirdPartApiWithSorting(): void
    {
        $gameId = 1;
        /** @var InMemoryGameScoreRepository $inMemoryGameScoreRepository */
        $inMemoryGameScoreRepository = self::$container->get(InMemoryGameScoreRepository::class);
        $inMemoryGameScoreRepository->addGameScore($this->creatGameScore(30, '-30 min'), $gameId);
        $inMemoryGameScoreRepository->addGameScore($this->creatGameScore(10, '-10 min'), $gameId);
        $inMemoryGameScoreRepository->addGameScore($this->creatGameScore(20, '-20 min'), $gameId);

        /** @var InMemoryCache $inMemoryCache */
        $inMemoryCache = self::$container->get(InMemoryCache::class);

        $gameScoreRepository = self::$container->get(GameScoreRepository::class);

        $result = $gameScoreRepository->findById($gameId, new GameScoreOrderBy('score', 'ASC'));
        $this->assertCount(3, $result);
        $this->assertEquals(10, $result->first()->getScore());
        $this->assertEquals($inMemoryCache->countItemInCache(), 1);
    }

    public function testGetGameResultFromCache(): void
    {
        $gameId = 1;

        /** @var InMemoryCache $inMemoryCache */
        $inMemoryCache = self::$container->get(InMemoryCache::class);
        $inMemoryCache->save(new CacheItem(
            'GameScore1',
            new ArrayCollection([$this->creatGameScore()]),
            new \DateTimeImmutable('+10 sec')
        ));

        $gameScoreRepository = self::$container->get(GameScoreRepository::class);

        $this->assertCount(1, $gameScoreRepository->findById($gameId, null));
    }

    public function testGetGameResultFromCacheWithSorting(): void
    {
        $gameId = 1;

        /** @var InMemoryCache $inMemoryCache */
        $inMemoryCache = self::$container->get(InMemoryCache::class);
        $inMemoryCache->save(new CacheItem(
            'GameScore1',
            new ArrayCollection([
                $this->creatGameScore(30, '-30 min'),
                $this->creatGameScore(10, '-10 min'),
                $this->creatGameScore(20, '-20 min')
            ]),
            new \DateTimeImmutable('+10 sec')
        ));

        $gameScoreRepository = self::$container->get(GameScoreRepository::class);

        $result = $gameScoreRepository->findById($gameId, new GameScoreOrderBy('score', 'ASC'));
        $this->assertCount(3, $result);
        $this->assertEquals(10, $result->first()->getScore());
    }


    public function testFetchNotExistingGamesResult(): void
    {
        $gameId = 1;

        /** @var InMemoryCache $inMemoryCache */
        $inMemoryCache = self::$container->get(InMemoryCache::class);
        $inMemoryCache->clear();

        $gameScoreRepository = self::$container->get(GameScoreRepository::class);

        $this->assertNull($gameScoreRepository->findById($gameId, null));
        $this->assertEquals($inMemoryCache->countItemInCache(), 0);
    }

    private function creatGameScore(int $score = 3, string $finishedAt = 'now'): GameScore
    {
        return new GameScore(Uuid::uuid4(), $score, new \DateTimeImmutable($finishedAt), new User(Uuid::uuid4(), 'User1'));
    }
}
