##development environment

### Requirement 
To set up a local environment following things are required:
* docker
* docker-compose
* make
    
To set up development environment run command
```bash
make setup
```

### troubleshooting
if your port 80 already taken you can change the port you which is exposed through environment variable "APP_WEB PORT"

##Static code analysis
To run **PHP mess detector** run command 
   ```bash
   make phpmd
   ```

To run **PHP stan** run command 
   ```bash
   make phpstan
   ```

To run **Easy Coding Standard** run command 
   ```bash
   make ecs
   ```

To fix coding standard run command 
   ```bash
   make ecs-fix
   ```

##Tests 

To run **Unit tests** run command 
   ```bash
   make phpspec-run
   ```

To run **Integration tests** run command 
   ```bash
   make phpunit
   ```

To run **Infrastructure tests** run command 
   ```bash
   make phpunit-infrastructure
   ```

To run **Infrastructure tests** run command 
   ```bash
   make phpunit-infrastructure
   ```
To run **functional tests** run command 
   ```bash
   make behat
   ```

#Build image and Deployment
Build image and Deployment should be made through bitbucket pipelines check https://bitbucket.org/PiotrBrzezina/recruitment/addon/pipelines/home

#Documentation
Api documentation is available at http://localhost